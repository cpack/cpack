#ifndef CPKSTORE_H
#define CPKSTORE_H

#include <QtCore>
#include <QtCore/QByteArray>
#include <QtCore/QMap>
#include <QtGui/QTreeView>

#pragma pack(push, 1)
typedef struct tagBlockRecItem {
    quint32 vID;
    quint32 Rev2;
    quint32 ParentvID;
    quint32 Offset;
    quint32 LzoSize;
    quint32 FileSize;
    quint32 RevFnLen;
} TBlockRecItem, *PBlockRecItem;

typedef struct tagVMBR {
    char Magic[3];      // RST
    uchar RevM;         // $1A
    quint32 Rev1;       // $01,00,00,00
    quint32 VFatOffset; // $80
    quint32 EmbOffset;  // $00E00080
    quint32 Rev2;       // $00,80,00,00
    quint32 BlockNum;   // Count of file + folder
    quint32 RevZ;       // $00,00,00,00
    quint32 RevVF1;     // $80
    quint32 BlockNum2;
    quint32 Rev3;       // $00,80,00,00
    quint32 RevZ2;      // $00,00,00,00
    quint32 CpkSize;    // Entire cpk length
    char RevNU[0x50];
} TVMBR, *PVMBR;
#pragma pack(pop)

typedef struct tagBlockRecItemEx : tagBlockRecItem {
    QByteArray ansifilename;
    tagBlockRecItemEx & operator = (const TBlockRecItem & other);
} TBlockRecItemEx, *PBlockRecItemEx;

typedef QMap < int, TBlockRecItemEx > TBlockRecs;

class CpkStore: public QObject {
    Q_OBJECT
protected:
    QByteArray fCachedBuf;
    TVMBR fVMBR;
    TBlockRecs fBlockRecs;
private:
    bool LoadFromFile(QIODevice* file, bool cached); // Parse? Assign? Cache?
public:
    bool AssignFile(QIODevice* file);
    void KeepFolderView(QTreeView* treeview);
};


#endif
