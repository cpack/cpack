#ifndef MAINUNT_H
#define MAINUNT_H

#include <QMainWindow>
#include "CpkStore.h"

namespace Ui {
    class TMainFrm;
}

class TMainFrm : public QMainWindow {
    Q_OBJECT
private:
    CpkStore fCpkStore;
public:
    TMainFrm(QWidget *parent = 0);
    ~TMainFrm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::TMainFrm *ui;
private slots:
    void onOpenCpkClicked();
};

#endif // MAINUNT_H
