#include "MainUnt.h"
#include "ui_MainFrm.h"
#include <QtGui/QFileDialog>
#include "DbCentre.h"

TMainFrm::TMainFrm(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TMainFrm)
{
    ui->setupUi(this);

    LoadAppSettings();
}

TMainFrm::~TMainFrm()
{
    SaveAppSettings();

    delete ui;
}

void TMainFrm::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void TMainFrm::onOpenCpkClicked()
{
    QString cpkfilename = QFileDialog::getOpenFileName(this, tr("Open SoftStar CPack File"), PathSetting.LastProjectFolder, "Excel Files (*.cpk);;All Files (*.*)", 0, 0);
    if (cpkfilename.isEmpty()) {
        return;
    }
    PathSetting.LastProjectFolder = QFileInfo(cpkfilename).path();
    QFile file(cpkfilename);
    file.open(QFile::ReadOnly);
    fCpkStore.AssignFile(&file);
    file.close();
}