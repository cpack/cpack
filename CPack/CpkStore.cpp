#include "CpkStore.h"
#include <QtCore/QVector>
#ifdef __GNUC__
#include <malloc.h>
#else
#include <windows.h>
#endif

bool CpkStore::LoadFromFile( QIODevice* file, bool cached )
{
    bool Result = false;

    QVector < TBlockRecItem > blocks;
    blocks.resize(fVMBR.BlockNum);
    file->read((char*)&blocks[0], sizeof(blocks[0]) * blocks.size());
    int endofblocks = file->pos();
    QMap < int, int > nameoffsets;

    int index = 0;
    foreach(TBlockRecItem item, blocks) {
        nameoffsets.insert(item.Offset, index);
        index++;
    }
    foreach(int idx, nameoffsets) {
        file->seek(blocks[idx].Offset + blocks[idx].LzoSize);
        QByteArray ansifilename;
        if (blocks[idx].RevFnLen == 0x38 + 2) {
            ansifilename = file->read(64);
        } else {
            ansifilename = file->read(blocks[idx].RevFnLen - (0x38 + 2));
        }
        TBlockRecItemEx item;
        item = blocks[idx];
        item.ansifilename = ansifilename;
        fBlockRecs.insert(idx, item);
    }

    return Result;
}

bool CpkStore::AssignFile( QIODevice* file )
{
    bool Result = false;
    // Cached = false?
    qint64 freemem = 0;
#ifdef __GNUC__
    freemem = _malloc_usable_size();
#else
    MEMORYSTATUSEX memstatus;
    GlobalMemoryStatusEx(&memstatus);
    freemem = memstatus.ullAvailPhys;
#endif
    TVMBR vmbr;
    file->read((char*)&vmbr, sizeof(vmbr));
    if ((*((quint32*)&vmbr.Magic[0]) & 0xFFFFFF) == 0x545352) {
        Result = true;
    } else {
        return Result;
    }

    // now we try to confirm it's me mario
    // don't use filesize because it's maybe a stream
    fVMBR = vmbr;
    bool cached = vmbr.CpkSize < freemem + fCachedBuf.size();
    if (cached) {
        
    } else {
        //file->setBuffer
    }
    Result = LoadFromFile(file, cached);
    
    return Result;
}

void CpkStore::KeepFolderView( QTreeView* treeview )
{

}

tagBlockRecItemEx & tagBlockRecItemEx::operator=( const TBlockRecItem & other )
{
    memcpy_s(this, sizeof(tagBlockRecItemEx), &other, sizeof(TBlockRecItem));
    return *this;
}